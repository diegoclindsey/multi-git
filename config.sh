## OVA Common ##
git submodule add http://www.agapa.junta-andalucia.es/gitlab/OVA/OVA_backendCommon_lib/SFW.git OvaCommonBackend
git submodule add http://www.agapa.junta-andalucia.es/gitlab/OVA/ova_frontendcommon_lib/sfw.git OvaCommonFrontend

### AGRO 360 ###
git submodule add http://www.agapa.junta-andalucia.es/gitlab/OVA/AGRO360/AGRO360_backend_ws/SFW.git Agro360Backend
git submodule add http://www.agapa.junta-andalucia.es/gitlab/OVA/AGRO360/AGRO360_BD/MODELO-DATOS Agro360ModeloDatos
git submodule add http://www.agapa.junta-andalucia.es/gitlab/OVA/AGRO360/AGRO360_BD/SCR.git Agro360Scripts
git submodule add http://www.agapa.junta-andalucia.es/gitlab/OVA/AGRO360/AGRO360_frontend_web/SFW.git Agro360Frontend

#### PINOCA ####
git submodule add http://www.agapa.junta-andalucia.es/gitlab/OVA/pinoca/pinoca_backend_ws/sfw.git PinocaBackend
git submodule add http://www.agapa.junta-andalucia.es/gitlab/OVA/pinoca/pinoca_bd/modelo-datos.git PinocaModeloDatos
git submodule add http://www.agapa.junta-andalucia.es/gitlab/OVA/pinoca/pinoca_bd/scr.git PinocaScripts
git submodule add http://www.agapa.junta-andalucia.es/gitlab/OVA/pinoca/pinoca_frontend_web/sfw.git PinocaFrontend
